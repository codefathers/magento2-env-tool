#!/usr/bin/env php
<?php


require_once __DIR__."/../../autoload.php";

use Cf\EnvTool\App;
use Cf\EnvTool\Arguments;
use Cf\EnvTool\Log\ConsoleLogger;

$logger = null;

try {
    $arguments = new Arguments($_SERVER['argv']);

    $minLevel = ($arguments->isVerbose()) ? ConsoleLogger::INFO : ConsoleLogger::DEBUG;
    $logger = new ConsoleLogger($minLevel);

    $app = App::factory($arguments);
    $app->run($logger);
} catch (Exception $e) {
    if ($logger) {
        $logger->error($e);
    } else {
        throw $e;
    }
}
