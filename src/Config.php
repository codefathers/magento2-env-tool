<?php


namespace Cf\EnvTool;

class Config
{
    /** @var int  */
    const PASSWORD_HASH_LENGTH = 20;

    /**
     * @var array
     */
    protected $data = array();

    /**
     * creates a new config object and auto configures
     * some pathes
     *
     * @param string $projectDir
     * @return Config
     */
    static public function factory($appDir, $configDir)
    {
        $data = array(
            'appDir' => $appDir,
            'configDir' => $configDir,
            'vendorDir' => self::joinPaths($appDir, 'vendor')
        );
        return new Config($data);
    }

    /**
     * Constructor
     *
     * @param array $data
     */
    public function __construct(Array $data = array())
    {
        $this->data = $data;
    }


    /**
     * @param string $key
     * @return mixed|string
     */
    protected function getData($key)
    {
        return $this->data[$key] ?? '';
    }


    /**
     * concats one or more pathes to a single path
     *
     * @return mixed|string
     */
    static protected function joinPaths()
    {
        $args = func_get_args();
        $result = "";
        foreach ($args as $arg) {
            if (strlen($arg)) {
                $result = $result . "/" . $arg;
            }
        }
        $result = str_replace("\\", "/", $result);
        $result = preg_replace('~/+~', '/', $result);
        return $result;
    }


    /**
     * returns the project Dir
     *
     * @param string $path
     * @return string
     */
    public function getAppPath($path = '')
    {
        return self::joinPaths($this->getData('appDir'), $path);
    }

    /**
     * returns the config dir
     *
     * @param string $path
     * @return string
     */
    public function getConfigPath($path = '')
    {
        return self::joinPaths($this->getData('configDir'), $path);
    }

    /**
     * returns the vendor path
     *
     * @param string $path
     * @return string
     */
    public function getVendorPath($path = '')
    {
        return self::joinPaths($this->getData('vendorDir'), $path);
    }
}