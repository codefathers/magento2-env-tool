<?php


namespace Cf\EnvTool;


use Cf\EnvTool\Exception;

/**
 * Mysql Database Connection
 *
 */
class DbConnection
{

    /** @var array */
    static public $instances = array();


    /** @var \PDO */
    protected $pdo = null;

    /**
     * @param array $params
     * @return DbConnection
     * @throws Exception
     *
     */
    static public function getInstance(Array $params)
    {
        ksort($params);
        $hash = md5(join('|', $params));
        if (isset(self::$instances[$hash])) {
            return self::$instances[$hash];
        }
        if (!isset($params['host'])) {
            throw new Exception("host is required");
        }
        if (!isset($params['database'])) {
            throw new Exception("database is required");
        }
        if (!isset($params['username'])) {
            throw new Exception("username is required");
        }
        if (!isset($params['password'])) {
            $params['password'] = '';
        }
        $pdo = new \PDO(
            "mysql:host={$params['host']};dbname={$params['database']};charset=utf8",
            $params['username'],
            $params['password'],
            array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
        );
        self::$instances[$hash] = $result = new DbConnection($pdo);
        $result->beginTransaction();
        return $result;
    }

    /**
     * DbConnection constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     *
     */
    public function beginTransaction()
    {
        if ($this->inTransaction()) {
            throw new Exception("already in transaction mode");
        }
        $this->pdo->beginTransaction();
    }

    /**
     * commits all changes when in transaction mode
     */
    public function commit()
    {
        if ($this->inTransaction()) {
            $this->pdo->commit();
        }
    }

    /**
     * rolls back the current transaction
     *
     */
    public function rollBack()
    {
        if ($this->inTransaction()) {
            $this->pdo->rollBack();
        }
    }

    /**
     * returns true if connection is currently
     * in transaction
     *
     * @return bool
     */
    public function inTransaction()
    {
        return $this->pdo->inTransaction() ? true : false;
    }


    /**
     * @return int
     */
    public function getLastInsertedId()
    {
        return $this->pdo->lastInsertId();
    }


    /**
     * @param string $sql
     * @param array $bind
     * @return array
     */
    public function fetchAll($sql, $bind = array())
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($bind);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        return $result;
    }


    /**
     * @param string $sql
     * @param array $bind
     * @return array
     */
    public function fetchRow($sql, $bind = array())
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($bind);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        if (count($result)>0) {
            return array_shift($result);
        }
        return array();
    }


    /**
     * @param string $sql
     * @param array $bind
     * @return array
     */
    public function fetchOne($sql, $bind = array())
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($bind);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetchColumn(0);
        return $result;
    }


    /**
     * @param string $sql
     * @param array $bind
     * @throws Exception
     */
    public function exec($sql, $bind = array())
    {
        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute($bind);
        if ($result === false) {
            throw new Exception("Error on sql command \n" . var_export($stmt->errorInfo(), true));
        }
    }

    /**
     *
     */
    static public function commitAll()
    {
        foreach (self::$instances as $connection) {
            $connection->inTransaction() && $connection->commit();
        }
    }

    /**
     *
     */
    static public function rollbackAll()
    {
        foreach (self::$instances as $connection) {
            $connection->inTransaction() && $connection->rollback();
        }
    }


}


