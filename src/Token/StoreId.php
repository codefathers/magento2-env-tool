<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Config;
use Cf\EnvTool\Environment;
use Cf\EnvTool\Exception;
use Cf\EnvTool\Helper;


/**
 * Class StoreId
 */
class StoreId extends AbstractToken
{


    /**
     * @return string
     */
    public function getId()
    {
        return "STORE_ID";
    }

    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function getValue($key)
    {
        $id = (int) $this->dbConnection()
            ->fetchOne("SELECT `store_id` FROM `store` WHERE `code` LIKE ?", array($key));
        if (!$id) {
            throw new Exception("Invalid Store code '$key'");
        }

        return $id;
    }



}
