<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Log\LogInterface;

interface TokenInterface
{

    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function getValue($key);


    /**
     * @return string
     */
    public function getId();

}