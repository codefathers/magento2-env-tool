<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Config;
use Cf\EnvTool\Environment;
use Cf\EnvTool\Exception;
use Cf\EnvTool\Helper;


/**
 * Class ThemeId
 */
class ThemeIdBe extends AbstractToken
{


    /**
     * @return string
     */
    public function getId()
    {
        return "BE_THEME_ID";
    }

    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function getValue($key)
    {
        $id = (int) $this->dbConnection()
            ->fetchOne("SELECT `theme_id` FROM `theme` WHERE `area` LIKE 'adminhtml' AND `code` LIKE ?", array($key));
        if (!$id) {
            throw new Exception("Invalid Admin-Theme Code '$key'");
        }
        return $id;
    }



}
