<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Exception;


/**
 * Class Env
 */
class Env extends AbstractToken
{

    /**
     * @return string
     */
    public function getId()
    {
        return "ENV";
    }


    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function getValue($key)
    {
        $result = $this->environment->getValue($key);
        if ($result == null) {
            throw new Exception("Undefined Environment-Var: '$key'");
        }
        return $result;
    }


}
