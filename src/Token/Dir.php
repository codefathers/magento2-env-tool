<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Exception;


/**
 * Class Dir
 */
class Dir extends AbstractToken
{


    /**
     * @return string
     */
    public function getId()
    {
        return "DIR";
    }

    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function getValue($key)
    {
        if ($key == 'mage') {
            return $this->config->getAppPath();
        } elseif ($key == 'vendor') {
            return $this->config->getVendorPath();
        } elseif ($key == 'config') {
            return $this->config->getConfigPath();
        } elseif ($key == 'sandbox') {
            $path = $this->config->getAppPath("dev/tests/integration/tmp/sandbox-*/etc/vendor_path.php");
            $files = glob($path);
            if (!empty($files)) {
                return dirname(dirname(array_shift($files)));
            }
            throw new Exception("sandbox path not found");
        } else {
            throw new Exception("invalid DIR type '$key'");
        }
    }


    /**
     *
     */
    protected function getSandboxDir()
    {
        $path = $this->config->getAppPath();
    }

}
