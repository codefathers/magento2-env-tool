<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Config;
use Cf\EnvTool\Environment;
use Cf\EnvTool\Exception;
use Cf\EnvTool\Helper;


/**
 * Class StoreGroupId
 */
class StoreGroupId extends AbstractToken
{


    /**
     * @return string
     */
    public function getId()
    {
        return "STORE_GROUP_ID";
    }

    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function getValue($key)
    {
        $id = (int) $this->dbConnection()
            ->fetchOne("SELECT `group_id` FROM `store_group` WHERE `code` LIKE ?", array($key));
        if (!$id) {
            throw new Exception("Invalid Store Group code '$key'");
        }

        return $id;
    }



}
