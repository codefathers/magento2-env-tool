<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Config;
use Cf\EnvTool\Environment;
use Cf\EnvTool\Exception;
use Cf\EnvTool\Helper;
use Cf\EnvTool\DbConnection;


/**
 * Class AbstractHandler
 * @package Cf\EnvTool\Handler
 */
abstract class AbstractToken implements TokenInterface
{

    /**
     * @var \Cf\EnvTool\Environment
     */
    protected $environment = null;

    /**
     * @var \Cf\EnvTool\Config
     */
    protected $config = null;

    /**
     * @var \Cf\EnvTool\Helper
     */
    protected $helper = null;


    /**
     * Constructor
     *
     * @param string $envFile
     * @throws Exception
     */
    public function __construct(Config $config, Environment $environment, Helper $helper)
    {
        $this->config = $config;
        $this->environment = $environment;
        $this->helper = $helper;
    }


    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    abstract public function getValue($key);

    /**
     * @return string
     */
    abstract public function getId();


    /**
     * @return DbConnection
     * @throws Exception
     */
    protected function dbConnection()
    {
        return DbConnection::getInstance($this->environment->getMySqlData());
    }


}
