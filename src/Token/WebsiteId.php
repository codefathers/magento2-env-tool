<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Config;
use Cf\EnvTool\Environment;
use Cf\EnvTool\Exception;
use Cf\EnvTool\Helper;


/**
 * Class WebsiteId
 */
class WebsiteId extends AbstractToken
{


    /**
     * @return string
     */
    public function getId()
    {
        return "WEBSITE_ID";
    }

    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function getValue($key)
    {
        $id = (int) $this->dbConnection()
            ->fetchOne("SELECT `website_id` FROM `store_website` WHERE `code` LIKE ?", array($key));
        if (!$id) {
            throw new Exception("Invalid Website code '$key'");
        }
        return $id;
    }



}
