<?php


namespace Cf\EnvTool\Token;

use Cf\EnvTool\Exception;
use Cf\EnvTool\Helper;


/**
 * Class Hash
 */
class Hash extends AbstractToken
{

    /**
     * @return string
     */
    public function getId()
    {
        return "HASH";
    }

    /**
     * returns a token value bases on given params
     *
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function getValue($key)
    {
        return $this->helper->createRandomHash($key);
    }



}
