<?php


namespace Cf\EnvTool\Log;

class ConsoleLogger extends AbstractLogger
{



    const COLORS = array(
        'black' => '0;30',
        'dark_gray' => '1;30',
        'blue' => '0;34',
        'light_blue' => '1;34',
        'green' => '0;32',
        'light_green' => '1;32',
        'cyan' => '0;36',
        'light_cyan' => '1;36',
        'red' => '0;31',
        'light_red' => '1;31',
        'purple' => '0;35',
        'light_purple' => '1;35',
        'brown' => '0;33',
        'yellow' => '1;33',
        'light_gray' => '0;37',
        'white' => '1;37'
    );


    const levelStrings = array(
        self::INFO => "Info",
        self::DEBUG => "Debug",
        self::NOTICE => "Notice",
        self::WARNING => "Warning",
        self::ERROR => "Error",
        self::SUCCESS => "Success",
    );

    const levelColors = array(
        self::INFO => "green",
        self::DEBUG => "cyan",
        self::NOTICE => "yellow",
        self::WARNING => "light_purple",
        self::ERROR => "red",
        self::SUCCESS => "green"
    );


    /**
     * @param string $color
     * @param string $str
     * @return string
     */
    protected function getColorString($color, $str)
    {
        $color = "\033[" . self::COLORS[$color] . "m";
        $colorNormal = "\033[0m";
        return "{$color}{$str}$colorNormal";
    }


    /**
     * @param int $level
     * @param string|\Exception $msg
     */
    protected function log($level, $msg)
    {
        if ($level < $this->minLevel) {
            return;
        }
        if ($msg instanceof \Exception) {
            $msg = $msg->getMessage();
            $level = self::ERROR;
        }
        if (is_array($msg)) {
            $msg = print_r($msg, true);
        }
        if ($level == self::SUCCESS) {
            $msg = $this->getColorString('green', $msg);
        } else {
            $levelStr = $this->getColorString(self::levelColors[$level], self::levelStrings[$level] . ":");
            $msg = "{$levelStr} {$msg}";
        }
        echo "{$msg}\n";
    }

}