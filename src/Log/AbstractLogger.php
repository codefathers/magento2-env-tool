<?php


namespace Cf\EnvTool\Log;

abstract class AbstractLogger implements LogInterface
{


    /** @var int  */
    protected $minLevel = self::INFO;



    const NONE = 0;
    const INFO = 1;
    const DEBUG = 2;
    const NOTICE = 3;
    const WARNING = 4;
    const ERROR = 5;
    const SUCCESS = 6;

    /**
     * Constructor
     *
     */
    public function __construct($minLevel = self::INFO)
    {
        $this->minLevel = $minLevel;
    }

    /**
     * @param int $level
     * @param string|\Exception $msg
     */
    abstract protected function log($level, $msg);

    /**
     * @param string|\Exception $msg
     */
    public function info($msg)
    {
        $this->log(self::INFO, $msg);
    }

    /**
     * @param string|\Exception $msg
     */
    public function debug($msg)
    {
        $this->log(self::DEBUG, $msg);
    }

    /**
     * @param string|\Exception $msg
     */
    public function notice($msg)
    {
        $this->log(self::NOTICE, $msg);
    }

    /**
     * @param string|\Exception $msg
     */
    public function warning($msg)
    {
        $this->log(self::WARNING, $msg);
    }

    /**
     * @param string|\Exception $msg
     */
    public function error($msg)
    {
        $this->log(self::ERROR, $msg);
    }

    /**
     * @param string|\Exception $msg
     */
    public function success($msg)
    {
        $this->log(self::SUCCESS, $msg);
    }

    /**
     * @param string|\Exception $msg
     */
    public function setMinLevel($level)
    {
        $this->minLevel = $level;
    }


}