<?php


namespace Cf\EnvTool\Log;

interface LogInterface
{


    /**
     * @param string|\Exception $msg
     */
    public function info($msg);

    /**
     * @param string|\Exception $msg
     */
    public function debug($msg);

    /**
     * @param string|\Exception $msg
     */
    public function notice($msg);

    /**
     * @param string|\Exception $msg
     */
    public function warning($msg);

    /**
     * @param string|\Exception $msg
     */
    public function error($msg);

    /**
     * @param string|\Exception $msg
     */
    public function success($msg);


}