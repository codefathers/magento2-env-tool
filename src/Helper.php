<?php


namespace Cf\EnvTool;


class Helper
{

    /** @var Config */
    private $config = null;

    /** @var Environment */
    private $environment = null;

    /** @var TokenProcessor */
    private $tokenProcessor = null;


    /**
     * Helper constructor.
     * @param Config $config
     * @param Environment $environment
     * @throws Exception
     */
    public function __construct(Config $config, Environment $environment)
    {
        $this->config = $config;
        $this->environment = $environment;
        $this->tokenProcessor = new TokenProcessor($this->config, $this->environment, $this);
    }


    /**
     * @param int $len
     * @return string
     */
    public function createRandomHash($len = 10)
    {
        $bytes = openssl_random_pseudo_bytes($len);
        $result = substr(bin2hex($bytes), 0, $len);
        return $result;
    }

    /**
     * returns true if given string value is int
     * @param string $val
     * @return bool
     */
    public function isInt($val)
    {
        $stringVal = trim((string)$val);
        if ($stringVal === "") {
            return false;
        }
        $intVal = (int)$stringVal;
        if ($intVal === 0) {
            return true;
        }
        if ((string)$intVal === $stringVal) {
            return true;
        }
        return false;
    }


    /**
     * @param $str
     * @return string
     * @throws Exception
     */
    public function replaceTokens($str)
    {
        return $this->tokenProcessor->replace($str);
    }




}