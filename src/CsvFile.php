<?php


namespace Cf\EnvTool;

class CsvFile
{


    /** @var array */
    protected $rows = null;

    /** @var array */
    protected $fields = null;

    /** @var int */
    protected $idx = -1;

    /** @var string */
    protected $env = 'default';

    /** @var Array */
    protected $groups = Array();

    /**
     * @var string
     */
    protected $fileName = '';


    /**
     * Constructor
     *
     * @param string $file
     * @throws Exception
     */
    public function __construct(String $file, String $env = null, Array $groups = array())
    {
        $this->clear();
        $this->env = $env;
        $this->groups = array_merge($groups);
        ($file) && $this->open($file);
        $this->first();
    }


    /**
     *
     */
    public function clear()
    {
        $this->idx = -1;
        $this->rows = array();
        $this->fields = array();
        $this->fileName = '';
    }


    /**
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @param string $file
     * @throws \Exception
     */
    public function open($fileName)
    {
        if (!strlen($fileName)) {
            throw new Exception("csv file is required");
        }
        if (!file_exists($fileName)) {
            throw new Exception("file not found: '$fileName'");
        }
        $lines = file($fileName);
        $this->parse($lines);
        $this->fileName = $fileName;
    }


    /**
     * parses as Array of csv lines
     *
     * @throws \Exception
     * @param string $file
     */
    protected function parse(Array $lines)
    {
        $this->clear();
        $items = array_map('str_getcsv', $lines);
        $idx = -1;
        $header = array();
        $rows = array();
        $lineNo = 0;
        foreach ($items as $item) {
            $idx++;
            $lineNo++;
            if (!$idx) {
                $header = $item;
                foreach ($header as $key => $name) {
                    $header[$key] = strtolower(trim($name));
                }
                continue;
            }
            $value = trim($item[0]);
            if (!strlen($value)) {
                continue;
            }
            if (strpos($value, '#') === 0) {
                continue;
            }
            $row = array(
                'line_no' => $lineNo
            );
            foreach ($header as $key => $name) {
                $row[$name] = ($key < count($item)) ? trim((string) $item[$key]) : '';
            }
            $value = (isset($row[$this->env])) ? $row[$this->env] : '';
            $row['value'] = (strlen($value) && $value !== '--skip--') ? $value : $row['default'];
            $rows[] = $row;
        }
        $this->fields = $header;
        $this->rows = $rows;
        $this->first();
    }


    /**
     * @return string
     */
    public function getFileName(): string
    {
       return $this->fileName;
    }

    /**
     * Return the current element
     * @return array|null $arr
     */
    public function current(): ?array
    {
        if (!$this->valid()) {
            return null;
        }
        return array_merge($this->rows[$this->idx]);
    }

    /**
     * Move forward to next element
     * @return $arr
     */
    public function first(): array
    {
        return $this->rewind();
    }

    /**
     * Move forward to next element
     * @return $arr
     */
    public function next(): ?array
    {
        $this->idx++;
        return $this->current();
    }

    /**
     * Rewind the Iterator to the first element
     * @return $arr
     *
     */
    public function rewind(): ?array
    {
        $this->idx = count($this->rows) ? 0 : -1;
        return $this->current();
    }

    /**
     * Return the key of the current element
     * @return int
     */
    public function key(): int
    {
        return (int) $this->idx;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     */
    public function valid(): bool
    {
        return ($this->idx >= 0 && $this->idx < count($this->rows)) ? true : false;
    }
}