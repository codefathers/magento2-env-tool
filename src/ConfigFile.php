<?php
/**
 * KUHN Product Configuraton System (PCS) Magento 2 Module
 * Copyright (c) 2017 KUHN Maßkonfektion KG
 * D-63936 Schneeberg
 * Website:    https://www.kuhn-masskonfektion.com
 * Contact:     info@kuhn-masskonfektion.com
 */

namespace Cf\EnvTool;



class ConfigFile
{

    /** @var array  */
    protected $arr = null;

    /** @var string  */
    protected $filename = null;

    /** @var array  */
    static protected $files = array();


    /**
     * @param $filename
     * @return mixed
     */
    static public function getInstance($filename)
    {
        if (!isset(self::$files[$filename])) {
            self::$files[$filename] = new self($filename);
        }
        return self::$files[$filename];
    }

    /**
     *
     */
    static public function saveAll()
    {
        foreach (self::$files as $file) {
            $file->save();
        }
    }

    /**
     * ConfigFile constructor.
     * @param string $filename
     */
    public function __construct($filename)
    {
        $this->close();
        if ($filename) {
            $this->load($filename);
        }
    }


    /**
     *
     */
    public function close()
    {
        $this->filename = null;
        $this->arr = null;
    }

    /**
     * @param string $filename
     * @throws Exception
     */
    protected function load($filename)
    {
        if (!file_exists($filename)) {
            throw new Exception("Invalid file name '$filename'");
        }
        $this->close();
        $this->arr = require($filename);
        $this->filename = $filename;
    }




    /**
     * @throws Exception
     */
    protected function save($filename = null)
    {
        $filename = ($filename) ? $filename : $this->filename;
        if (!$filename) {
            throw new Exception("filename is required");
        }
        if (!$this->arr) {
            throw new Exception("no file loaded");
        }
        file_put_contents(
            $filename,
            "<?php\nreturn " . var_export($this->arr, true) . ";\n"
        );
    }


    /**
     * @param string $path
     * @param string $value
     * @throws Exception
     */
    public function setValue($path, $value)
    {
        $link = &$this->arr;
        $keys = explode('.', $path);
        foreach ($keys as $idx => $key) {
            if (!isset($link[$key])) {
                $link[$key] = array();
            }
            if ($idx<count($keys)-1 && !is_array($link[$key])) {
                throw new Exception("Can not set value for `$key` in `$path` path because it is not array.");
            }

            $link = &$link[$key];
        }
        if ((string) (int) $value == $value) {
            $value = (int) $value;
        }
        $link = $value;
    }

}