<?php


namespace Cf\EnvTool;

use Cf\EnvTool\Config;
use Cf\EnvTool\Environment;
use Cf\EnvTool\Token;
use Cf\EnvTool\Token\TokenInterface;

class TokenProcessor
{

    /**
     * @var \Cf\EnvTool\Environment
     */
    protected $environment = null;

    /**
     * @var \Cf\EnvTool\Config
     */
    protected $config = null;


    /**
     * @var \Cf\EnvTool\Helper
     */
    protected $helper = null;


    /** @var array */
    protected $token = array();


    /**
     * Constructor
     *
     * @param string $envFile
     * @throws Exception
     */
    public function __construct(Config $config, Environment $environment, Helper $helper)
    {
        $this->config = $config;
        $this->environment = $environment;
        $this->helper = $helper;
        $this->token = array();
        $this->register($this->createToken('Env'));
        $this->register($this->createToken('Dir'));
        $this->register($this->createToken('Hash'));
        $this->register($this->createToken('StoreId'));
        $this->register($this->createToken('WebsiteId'));
        $this->register($this->createToken('StoreGroupId'));
        $this->register($this->createToken('ThemeId'));
        $this->register($this->createToken('ThemeIdBe'));
    }

    /**
     * @param $type
     * @return TokenInterface
     */
    protected function createToken($type)
    {
        $class = "Cf\\EnvTool\\Token\\$type";
        $result = new $class($this->config, $this->environment, $this->helper);
        return $result;
    }

    /**
     * @param TokenInterface $token
     */
    public function register(TokenInterface $token)
    {
        $id = strtolower($token->getId());
        $this->token[$id] = $token;
    }

    /**
     * @param string $type
     * @return mixed
     * @throws Exception
     */
    protected function get($type)
    {
        $t = strtolower(trim((string) $type));
        if (!isset($this->token[$t])) {
            throw new Exception("Unknown token '$type'");
        }
        return $this->token[$t];
    }


    /**
     * replaces all tokens in a given string
     *
     * @param string $str
     * @return string
     * @throws Exception
     */
    public function replace($str)
    {
        $matches = array();
        preg_match_all('/###\s?([a-zA-Z0-9_]+):\s?([^#]+)\s?###/i', $str, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $token = $this->get($match[1]);
            $value = $token->getValue($match[2]);
            $str = str_replace($match[0], $value, $str);
        }
        return $str;
    }


}