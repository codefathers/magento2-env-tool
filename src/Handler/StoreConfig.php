<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Log\LogInterface;

use Cf\EnvTool\Exception;
use Magento\Store\Model\ScopeInterface;

class StoreConfig extends DbAbstract
{

    /** @var */
    static protected $validScopes;


    /**
     * @param LogInterface|null $logger
     * @throws \Cf\EnvTool\Exception
     */
    protected function _apply(LogInterface $logger = null)
    {

        $scope = strtolower(trim((string)$this->getParam2()));
        $scope = $scope ? $scope : 'default';
        if (!self::isValidScope($scope)) {
            throw new Exception("Invalid Scope - must be one of " . join(',', $this->getValidScopes()));
        }

        $scopeId = $this->getParam3();
        switch ($scope) {
            case (ScopeInterface::SCOPE_WEBSITES):
                $scopeId = ($this->isInt($scopeId)) ? (int)$scopeId : (int)$this->getWebsiteId($scopeId);
                break;
            case (ScopeInterface::SCOPE_STORES):
                $scopeId = ($this->isInt($scopeId)) ? (int)$scopeId : (int)$this->getStoreId($scopeId);
                break;
            case (ScopeInterface::SCOPE_GROUPS):
                $scopeId = ($this->isInt($scopeId)) ? (int)$scopeId : (int)$this->getGroupId($scopeId);
                break;
            default:
                $scopeId = 0;
                break;
        }

        $path = (string)$this->getParam1();
        $value = (string)$this->getValue();

        switch (trim($value)) {
            case ('--delete--'):
                $this->delete($scope, $scopeId, $path);
                $logger->info("delete store config: $path => $value ($scope, $scopeId)");
                break;
            case ('--/--'):
            case ('--empty--'):
                $logger->info("store config: $path => '' ($scope, $scopeId)");
                $this->insert($scope, $scopeId, $path, "");
                break;
            default:
                $this->insert($scope, $scopeId, $path, $value);
                $logger->info("store config: $path => $value ($scope, $scopeId)");
                break;
        }
    }

    /**
     * @param string $scope
     * @param string $scopeId
     * @param string $path
     * @param string $value
     *
     * @throws \Cf\EnvTool\Exception
     */
    protected function insert($scope, $scopeId, $path, $value)
    {
        $scopeId = (int)$scopeId;
        /*if (!in_array($scopeId, [0, 1, 2])) {
            throw new Exception("invalid scope id '$scopeId'");
        }*/
        $parts = explode('/', $path);
        if (count($parts) < 3) {
            throw new Exception("invalid path '$path'");
        }
        $bind = array(
            ':scope' => (string)$scope,
            ':scope_id' => (int)$scopeId,
            ':path' => (string)$path,
            ':value' => (string)$value
        );
        $sql = "INSERT INTO core_config_data (`scope`,`scope_id`,`path`,`value`) 
                  values (:scope, :scope_id, :path, :value) 
                  ON DUPLICATE KEY UPDATE `value`= :value;";
        $this->exec($sql, $bind);
    }

    /**
     * @param string $scope
     * @param string $scopeId
     * @param string $path
     * @throws \Cf\EnvTool\Exception
     *
     */
    protected function delete($scope, $scopeId, $path)
    {
        if (!strlen($scope)) {
            throw new Exception("a scope is required");
        }
        if (!strlen($scopeId)) {
            throw new Exception("a scope is required");
        }
        $bind = array(
            ':scope' => (string)$scope,
            ':scope_id' => (string)$scopeId,
            ':path' => (string)$path
        );
        $sql = "DELETE FROM core_config_data WHERE 
                    `scope` like :scope AND 
                    `scope_id` like :scope_id AND 
                    `path` like :path;";
        $this->exec($sql, $bind);
    }


    /**
     * returns website id of a given website code
     * @param string $code
     * @return int
     */
    protected function getWebsiteId($code)
    {
        return (int)$this->fetchOne("SELECT `website_id` FROM `store_website` WHERE `code` LIKE ?", array($code));
    }


    /**
     * returns store id of a given store code
     * @param string $code
     * @return bool
     */
    protected function getStoreId($code)
    {
        return (int)$this->fetchOne("SELECT `store_id` FROM `store` WHERE `code` LIKE ?", array($code));
    }


    /**
     * returns group id of a given group code
     * @param string $code
     * @return bool
     */
    protected function getGroupId($code)
    {
        return (int)$this->fetchOne("SELECT `group_id` FROM `store` WHERE `code` LIKE ?", array($code));
    }


    /**
     * returns true if given string value is int
     * @param string $val
     * @return bool
     */
    protected function isInt($val)
    {
        return $this->helper->isInt($val);
    }

    /**
     * returns true if given scope is valid
     * @param $scope
     * @return int
     */
    static protected function isValidScope($scope)
    {
        return in_array($scope, self::getValidScopes()) ? true : false;
    }

    /**
     * @return array
     */
    static protected function getValidScopes()
    {
        if (!self::$validScopes) {
            self::$validScopes = array(
                'default',
                ScopeInterface::SCOPE_STORES,
                ScopeInterface::SCOPE_GROUPS,
                ScopeInterface::SCOPE_WEBSITES
            );
        }
        return self::$validScopes;
    }


}