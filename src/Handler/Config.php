<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Log\LogInterface;

class Config extends ConfigFile
{


    /**
     * @param LogInterface|null $logger
     */
    protected function _apply(LogInterface $logger = null)
    {
        $file = $this->getFile('###DIR:mage###/app/etc/config.php');
        $path = $this->getParam1();
        $value = $this->getValue();
        $logger->info("(###DIR:mage###/app/etc/config.php): $path => $value");
        $file->setValue($path, $value);
    }
}