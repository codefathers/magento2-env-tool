<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Log\LogInterface;

interface HandlerInterface
{

    public function apply(LogInterface $logger = null);

}