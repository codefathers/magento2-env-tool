<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Log\LogInterface;
use Cf\EnvTool\Exception;


class FileCopy extends AbstractHandler
{


    /**
     * copies a file from
     *    src: value
     *    dst: param1
     *
     * @param LogInterface|null $logger
     * @throws Exception
     */
    protected function _apply(LogInterface $logger = null)
    {
        $srcFile = $this->getValue();
        if (!strlen($srcFile)) {
            /* nothing todo - skip */
            return;
        }
        if (!file_exists($srcFile)) {
            throw new Exception ("invalid source file '$srcFile'");
        }
        $dstFile = $this->getParam1();
        if (!strlen($dstFile)) {
            throw new Exception ("destination file is required");
        }
        $ifNotExists = (int) $this->getParam2();
        if ($ifNotExists && file_exists($dstFile)) {
            return;
        }
        $logger->info("fileCopy: $srcFile => $dstFile");
        $result = copy($srcFile, $dstFile);
        if ($result !== true) {
            throw new Exception ("file copy failed: '$srcFile' => '$dstFile'");
        }
        chmod($dstFile,0664);
    }

}