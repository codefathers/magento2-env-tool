<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Exception;
use Cf\EnvTool\Log\LogInterface;



class ConfigFile extends AbstractHandler
{

    /** @var string  */
    protected $file = null;

    /**
     * @param string $filename
     * @throws Exception
     * @return \Cf\EnvTool\ConfigFile
     */
    protected function getFile($filename)
    {
        $filename = $this->replaceTokens($filename);
        if (!file_exists($filename)) {
            throw new Exception("Invalid file name '$filename'");
        }
        $result = \Cf\EnvTool\ConfigFile::getInstance($filename);
        return $result;
    }


    /**
     * assigns the config value to path in array file
     * and saves it
     *
     * @param LogInterface|null $logger
     */
    protected function _apply(LogInterface $logger = null)
    {
        $filename = $this->getParam1();
        $file = $this->getFile($filename);
        $path = $this->getParam2();
        $value = $this->getValue();
        $logger->info("($filename): $path => $value");
        $file->setValue($path, $value);
    }




}