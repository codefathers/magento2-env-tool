<?php
/**
 * KUHN Product Configuraton System (PCS) Magento 2 Module
 * Copyright (c) 2017 KUHN Maßkonfektion KG
 * D-63936 Schneeberg
 * Website:    https://www.kuhn-masskonfektion.com
 * Contact:     info@kuhn-masskonfektion.com
 */

namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Exception;
use Cf\EnvTool\Log\LogInterface;


class FilePermissions extends AbstractHandler
{

    /**
     * @param LogInterface|null $logger
     * @throws \Cf\EnvTool\Exception
     */
    protected function _apply(LogInterface $logger = null)
    {
        $flags = $this->getValue();
        $fileName = $this->getParam1();
        if (!file_exists($fileName)) {
            throw new Exception("file not found '$fileName'");
        }
        $logger->info("Update File Permissions $fileName::$flags");
        $cmd = "chmod $flags $fileName";
        shell_exec($cmd);
    }


}