<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Log\LogInterface;


class Environment extends ConfigFile
{


    /**
     * @param LogInterface|null $logger
     */
    protected function _apply(LogInterface $logger = null)
    {
        $file = $this->getFile('###DIR:mage###/app/etc/env.php');
        $path = $this->getParam1();
        $value = $this->getValue();
        $logger->info("(env): $path => $value");
        $file->setValue($path, $value);
    }


}