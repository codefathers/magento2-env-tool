<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Log\LogInterface;
use Cf\EnvTool\Exception;


class DeleteFile extends AbstractHandler
{


    /**
     * copies a file from
     *    src: value
     *    dst: param1
     *
     * @param LogInterface|null $logger
     * @throws Exception
     */
    protected function _apply(LogInterface $logger = null)
    {
        $filePath = $this->getValue();
        if (!strlen($filePath)) {
            /* nothing todo - skip */
            return;
        }
        if (file_exists($filePath)) {
            $logger->info("($filePath): delete");

            unlink($filePath);
        }
    }

}