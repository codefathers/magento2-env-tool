<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Config;
use Cf\EnvTool\Environment;
use Cf\EnvTool\Exception;
use Cf\EnvTool\Log\LogInterface;
use Cf\EnvTool\Helper;


/**
 * Class AbstractHandler
 * @package Cf\EnvTool\Handler
 */
abstract class AbstractHandler implements HandlerInterface
{

    /** @var Helper */
    protected $helper = null;

    /** @var array */
    protected $data = null;

    /** @var Environment */
    protected $environment = null;

    /** @var Config */
    protected $config = null;

    /**
     *
     * creates a hander for a given csv row (data)
     * @param array $data
     * @param Config $config
     * @param Environment $environment
     * @return AbstractHandler
     */
    static public function factory(Config $config, Environment $environment, Helper $helper, Array $data)
    {
        $class = "Cf\\EnvTool\\Handler\\" . $data['handler'];
        $result = new $class($data, $config, $environment, $helper);
        return $result;
    }

    /**
     * AbstractHandler constructor.
     * @param array $data
     * @param Config $config
     * @param Environment $environment
     */
    public function __construct(Array $data, Config $config, Environment $environment, Helper $helper)
    {
        $this->data = $data;
        $this->config = $config;
        $this->environment = $environment;
        $this->helper = $helper;
    }

    /**
     * returns my data array
     *
     * @return array
     */
    public function getData()
    {
        return array_merge($this->data);
    }

    /**
     * @param string $key
     * @return string
     * @throws Exception
     */
    protected function getValue($key = 'value')
    {
        $result = (isset($this->data[$key])) ? $this->data[$key] : '';
        $result = (string)$this->replaceTokens($result);
        return (string)$result;
    }


    /**
     * @return string
     * @throws Exception
     */
    protected function getHandler()
    {
        return (string)$this->getValue('handler');
    }


    /**
     * @return string
     * @throws Exception
     */
    protected function getParam1()
    {
        return (string)$this->getValue('param1');
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function getParam2()
    {
        return (string)$this->getValue('param2');
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function getParam3()
    {
        return (string)$this->getValue('param3');
    }


    /**
     * @param LogInterface|null $logger
     * @throws \Exception
     */
    final public function apply(LogInterface $logger = null)
    {
        try {
            if ($this->getValue() == '--skip--') {
                return;
            }
            $this->beforeApply($logger);
            $this->_apply($logger);
            $this->afterApply($logger);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * is called right before _apply will be called
     */
    protected function beforeApply(LogInterface $logger = null)
    {
        // to override
    }


    abstract protected function _apply(LogInterface $logger = null);


    /**
     * is called just after _apply will be called
     */
    protected function afterApply(LogInterface $logger = null)
    {
        // to override
    }


    /**
     * replaces all known tokens in a given string
     *
     * @param $str
     * @return string
     * @throws Exception
     */
    public function replaceTokens($str)
    {
        return $this->helper->replaceTokens($str);
    }

}
