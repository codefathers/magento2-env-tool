<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\DbConnection;
use Cf\EnvTool\Config;


abstract class DbAbstract extends AbstractHandler
{

    /**
     * @return DbConnection
     * @throws \Cf\EnvTool\Exception
     */
    protected function dbConnection()
    {
        return DbConnection::getInstance($this->environment->getMySqlData());
    }


    /**
     * @param string $sql
     * @param array $bind
     * @return array
     * @throws \Cf\EnvTool\Exception
     */
    protected function fetchAll($sql, $bind = array())
    {
        return $this->dbConnection()->fetchAll($sql, $bind);
    }


    /**
     * @param string $sql
     * @param array $bind
     * @return array
     * @throws \Cf\EnvTool\Exception
     */
    protected function fetchRow($sql, $bind = array())
    {
        return $this->dbConnection()->fetchRow($sql, $bind);
    }


    /**
     * @param string $sql
     * @param array $bind
     * @return array
     * @throws \Cf\EnvTool\Exception
     */
    protected function fetchOne($sql, $bind = array())
    {
        return $this->dbConnection()->fetchOne($sql, $bind);
    }

    /**
     * @param string $sql
     * @param array $bind
     * @throws \Cf\EnvTool\Exception
     */
    protected function exec($sql, $bind = array())
    {
        $this->dbConnection()->exec($sql, $bind);
    }


    /**
     * @param $password
     * @return array
     * @throws \Cf\EnvTool\Exception
     */
    protected function encryptPassword($password)
    {
        $salt = $this->helper->createRandomHash(Config::PASSWORD_HASH_LENGTH);
        $result = $this->fetchOne("select CONCAT(SHA2('{$salt}{$password}', 256), ':{$salt}:1');");
        return $result;
    }


}