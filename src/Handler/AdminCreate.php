<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Log\LogInterface;

use Cf\EnvTool\Exception;

class AdminCreate extends DbAbstract
{

    /**
     * @param LogInterface|null $logger
     * @throws \Cf\EnvTool\Exception
     */
    protected function _apply(LogInterface $logger = null)
    {
        $name = $this->getValue();
        if ($this->adminUserExists($name)) {
            return;
        }
        $password = $this->helper->createRandomHash(20);
        $email = $this->helper->createRandomHash(20) . "@" . $this->environment->getValue('DOMAIN');
        $logger->info("Create admin user '$name'");
        $this->createAdminUser($name, $name, $email, $name, $password);
    }

    /**
     * returns true if admin user with given name exists
     *
     * @param $name
     */
    protected function adminUserExists($name)
    {
        $bind = [$name];
        $id = (int)$this->fetchOne("SELECT `user_id` FROM `admin_user` WHERE `username` = ?", $bind);
        return ($id) ? true : false;
    }


    /**
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $username
     * @param string $password
     */
    protected function createAdminUser($firstname, $lastname, $email, $username, $password)
    {
        $pw = $this->encryptPassword($password);
        $sql = "INSERT INTO `admin_user` (`firstname`,`lastname`,`email`,`username`,`password`) VALUES (?,?,?,?,?)";
        $bind = [$firstname, $lastname, $email, $username, $pw];
        $this->exec($sql, $bind);
    }



}