<?php


namespace Cf\EnvTool\Handler;

use Cf\EnvTool\Log\LogInterface;

use Cf\EnvTool\Exception;

class AdminPassword extends DbAbstract
{

    /**
     * @param LogInterface|null $logger
     * @throws \Cf\EnvTool\Exception
     */
    protected function _apply(LogInterface $logger = null)
    {
        $name = $this->getParam1();
        if (!$this->adminUserExists($name)) {
            $logger->error("Admin user '$name' not found, can't apply password");
        }
        $password = (string) $this->getValue();
        if (empty($password)) {
            $logger->error("Password for user '$name' is empty");
        }
        $logger->info("Update Password for user '$name'");
        $this->updatePassword($name, $password);
    }


    /**
     * returns true if admin user with given name exists
     *
     * @param $name
     */
    protected function adminUserExists($name)
    {
        $bind = [$name];
        $id = (int)$this->fetchOne("SELECT `user_id` FROM `admin_user` WHERE `username` = ?", $bind);
        return ($id) ? true : false;
    }


    /**
     * @param $name
     * @param $password
     */
    protected function updatePassword($name, $password)
    {
        $pw = $this->encryptPassword($password);
        $sql = "UPDATE `admin_user` SET `password` = ? WHERE `username` = ?";
        $bind = [$pw, $name];
        $this->exec($sql, $bind);
    }



}