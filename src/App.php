<?php

namespace Cf\EnvTool;

use Cf\EnvTool\Log\LogInterface;
use Cf\EnvTool\Log\ConsoleLogger;
use Cf\EnvTool\Handler\AbstractHandler;
use Cf\EnvTool\Exception\NoticeException;
use Cf\EnvTool\DbConnection;

final class App
{


    /** @var Environment */
    private $environment = null;

    /** @var CsvFile */
    private $csvFile = null;

    /** @var Config */
    private $config = null;

    /** @var Helper */
    private $helper = null;

    /**
     * creates a env settings application depending on
     * given arguments. Preconfigure the app with
     * settings and project related values
     *
     * @throws Exception
     * @param Arguments $arguments
     * @return App
     */
    static function factory(Arguments $arguments)
    {
        $appDir = trim((string) $arguments->getAppDir());
        $appDir = $appDir ? $appDir : '/srv';

        if (!$appDir || !file_exists($appDir.'/composer.json')) {
            throw new Exception ("Invalid app dir '$appDir'");
        }

        $configDir = trim((string) $arguments->getConfigDir());
        $configDir = $configDir ? : "$appDir/.config";

        $environment = new Environment();
        $config = Config::factory($appDir, $configDir);

        $settingsFile = $arguments->getSettingsFile();
        if (!$settingsFile) {
            $settingsFile = 'settings-base.csv';
        }
        $settingsFile = $config->getConfigPath("/magento/$settingsFile");
        if (!file_exists($settingsFile)) {
            throw new Exception ("missing settings file: '$settingsFile'");
        }

        $env = $arguments->getEnv();
        $env = $env ?? $environment->getEnv();
        $groups = $arguments->getGroups();
        $csvFile = new CsvFile($settingsFile, $env, $groups);

        $result = new App($config, $environment, $csvFile);
        return $result;
    }


    /**
     * App constructor.
     * @param Config $config
     * @param Environment $environment
     * @param CsvFile $csvFile
     * @throws Exception
     */
    public function __construct(Config $config, Environment $environment, CsvFile $csvFile)
    {
        $this->config = $config;
        $this->environment = $environment;
        $this->csvFile = $csvFile;
        $this->helper = new Helper($this->config, $this->environment);
    }

    /**
     *
     * @param LogInterface $logger
     * @throws Exception
     */
    public function run(LogInterface $logger = null)
    {
        if (!$logger) {
            $logger = new ConsoleLogger();
        }
        $verbose = (int) $this->environment->getValue('VERBOSE');
        if ($verbose) {
            $logger->setMinLevel($logger::INFO);
        }
        try {
            $data = $this->csvFile->first();
            while ($data) {
                $handler = $this->createHandler($data);
                try {
                    $handler->apply($logger);
                } catch (\Exception $ex) {
                    $msg = array(
                        'message' => $ex->getMessage(),
                        'fileName' => $this->csvFile->getFileName(),
                        'data' => $handler->getData()
                    );
                    $logger->error($msg);
                    throw new NoticeException("process canceled");
                }
                $data = $this->csvFile->next();
            }
            ConfigFile::saveAll();
            DbConnection::commitAll();
            $env = $this->csvFile->getEnv();
            $fileName = basename($this->csvFile->getFileName());
            $logger->success("Success - Environment '$env:$fileName' assigned");
        } catch (NoticeException $e) {
            $logger->notice($e->getMessage());
            $this->undoChanges();
        } catch (\Exception $e) {
            $logger->error($e);
            $this->undoChanges();
        }
    }


    /**
     * rollback all database changes
     */
    protected function undoChanges()
    {
        DbConnection::rollbackAll();
    }

    /**
     * @param Array $data
     * @return AbstractHandler
     */
    protected function createHandler(Array $data)
    {
        return AbstractHandler::factory($this->config, $this->environment, $this->helper, $data);
    }


}