<?php


namespace Cf\EnvTool;

class Arguments
{
    /**
     * @var array
     */
    protected $args = null;

    /**
     * Constructor
     *
     * @param arr $arr
     */
    public function __construct($arr)
    {
        $this->args = getopt("a:c:s:e:g:v");
    }

    private function getArgument($key): ?string
    {
        return $this->args[$key] ?? null;
    }

    /**
     * @return string|null
     */
    public function getAppDir(): ?string
    {
        return $this->getArgument('a');
    }

    /**
     * @return string|null
     */
    public function getConfigDir(): ?string
    {
        return $this->getArgument('c');
    }

    /**
     * @return string|null
     */
    public function getSettingsFile(): ?string
    {
        return $this->getArgument('s');
    }

    /**
     * @return string|null
     */
    public function getEnv(): ?string
    {
        return $this->getArgument('e');
    }

    /**
     * @return string|null
     */
    public function getGroups(): array
    {
        $items = $this->getArgument('g');
        return ($items) ? explode(',', $items) : [];
    }

    /**
     * @return boolean
     */
    public function isVerbose(): bool
    {
        return !!$this->getArgument('v');
    }
}