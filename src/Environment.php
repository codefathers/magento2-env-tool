<?php


namespace Cf\EnvTool;

class Environment
{
    public function getValue(string $key): ?string
    {
        $result = getenv($key);
        return ($result === false) ? null : (string) $result;
    }

    public function getEnv()
    {
        return $this->getValue('ENV');
    }

    public function getMySqlData(): array
    {
        return array(
            'host' => $this->getValue('MYSQL_HOST'),
            'database' => $this->getValue('MYSQL_DATABASE'),
            'username' => $this->getValue('MYSQL_USER'),
            'password' => $this->getValue('MYSQL_PASSWORD'),
            'port' => '3306'
        );
    }

    public function getProjectName(): string
    {
        return $this->getValue('COMPOSE_PROJECT_NAME');
    }
}